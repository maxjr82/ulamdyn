"""The :mod:`ulamdyn.data_loader` module is used to build datasets from NAMD
output files."""

# Author: Max Pinheiro Jr <maxjr82@gmail.com>
# Date: March 10 2021
from __future__ import (
    absolute_import,
    annotations,
    division,
    print_function,
    unicode_literals,
    with_statement,
)

import os
from itertools import combinations, product
from typing import Tuple

import h5py
import numpy as np
import rmsd

try:
    import modin.pandas as pd

except ModuleNotFoundError:
    import pandas as pd

from ulamdyn.base import BaseClass
from ulamdyn.nx_utils import (
    BOHR_TO_ANG,
    HARTREE_TO_eV,
    check_nx_trajs,
    get_nx_version,
    read_h5_nx,
    read_nx_control,
)

__all__ = ["GetCoords", "GetGradients", "GetCouplings", "GetProperties"]


# %% Starting the first class: GetCoords
class GetCoords(BaseClass):
    """Read the Cartesian coordinates from Newton-X MD trajectories.

    .. note:: In the case of NX classical series, the Cartesian XYZ coordinates
              can be read either from dyn.out or dyn.xyz. For NX new series,
              the data will be read from the h5 file available in each TRAJ
              directory. Repeated geometries are skipped.

    It also provides a function to calculate the root-mean-squared deviation
    (RMSD) between each geometry read from the MD trajectories and a reference
    geometry. Before calculating the RMSD, the two geometries are aligned using
    the Kabsch algorithm (https://en.wikipedia.org/wiki/Kabsch_algorithm).

    This class does not require arguments in its constructor. All the outputs
    generate by the class are given in angstroms.

    Data attributes:
    ----------------
       ``trajectories`` (dict): contain indices of the trajectories available
                                with the respective maximum time to read.\n
       ``labels`` (numpy.ndarray): stores the sequence of atom labels.\n
       ``eq_xyz`` (numpy.ndarray): stores the XYZ matrix of the reference
                                   geometry (geom.xyz).\n
       ``xyz`` (numpy.ndarray): stores the XYZ matrices of all geometries read
                                from the TRAJ directories.\n
       ``rmsd`` (numpy.ndarray): vector of RMSD values between all geometries
                                 and the reference one.\n
       ``dataset`` (pandas.dataframe): stores a dataframe of the flattened
                                       XYZ matrices.
    """

    # Defining slots to optimize performance (RAM):
    __slots__ = [
        "trajectories",
        "labels",
        "eq_xyz",
        "xyz",
        "rmsd",
        "dataset",
        "traj_time",
    ]

    def __init__(self) -> None:
        """Class initialization."""
        # This variable contains a dictionary storing the available
        # trajectories as keys together with the corresponding t_max:
        # {'TRAJ1': tmax_1, 'TRAJ2': tmax_2,..., 'TRAJN': tmax_n}
        self.trajectories = check_nx_trajs()
        # Store the sequence of atom labels as a numpy array
        self.labels = None
        # Store the XYZ matrix of the reference geometry (np.array)
        self.eq_xyz = None
        # Try to read the reference geometry, geom.xyz file
        # If the file is available, the labels and eq_xyz variables
        # will be update with the data loaded by the function below.
        self.read_eq_geom()

        self.xyz = None
        self.rmsd = None
        self.dataset = None
        self.traj_time = None

    def __len__(self):
        if self.xyz is not None:
            return len(self.xyz)

    def __getitem__(self, idx) -> str:
        if self.xyz is not None:
            n_atoms = len(self.labels)
            geom_string = ""

            if isinstance(idx, int):
                traj, time = self.traj_time[idx]
                selected_geom = self.xyz[idx]
            elif isinstance(idx, tuple) and len(idx) == 2:
                traj, time = idx
                c1 = self.traj_time[:, 0] == traj
                c2 = self.traj_time[:, 1] == time
                selected_geom = self.xyz[(c1 & c2)][0]
            else:
                print("ERROR: Invalid input!")
                print("The input must be either a tuple with TRAJ and time")
                print("values or a single integer corresponding to the data")
                print("index.")
                return geom_string

            comment_line = f"TRAJ = {traj}  |  time = {time}"
            geom_string = str(n_atoms) + "\n" + comment_line + "\n"
            mask = "{:<6s} {:12.8f} {:12.8f} {:12.8f} \n"
            for label, atom_coords in zip(self.labels, selected_geom):
                geom_string += mask.format(label[0], *atom_coords)
            return geom_string

    def save_csv(self) -> None:
        """Save all loaded geometries (raw format) into a csv file.

        If the RMSD has been calculated, it will be included as an extra
        column in the XYZ coordinates data set.

        The default name of the output file is all_coordinates.csv.
        """
        if self.dataset is None:
            self.build_dataframe()

        df = self.dataset
        df.to_csv("all_coordinates.csv", index=False, header=True)

    def _insert_traj_time(self, df):
        traj_time_in_cols = all(
            col in df.columns.tolist() for col in ["TRAJ", "time"]
        )
        # Check first if the TRAJ and time columns already exist in dataframe
        if not traj_time_in_cols:
            if self.traj_time is not None:
                df.insert(0, "TRAJ", self.traj_time[:, 0])
                df.insert(1, "time", self.traj_time[:, 1])
                df["TRAJ"] = df["TRAJ"].astype(int)
        return df

    def build_dataframe(self) -> None:
        """Create a DataFrame containing flattened XYZ coordinates from all
        MD trajectories.

        After running this function, the class attribute
        :attr:`~ulamdyn.GetCoords.dataset` will be updated with the loaded
        DataFrame object.
        """
        if all(v is not None for v in [self.labels, self.xyz]):
            n_atoms = len(self.labels)
            col_names = [
                ["x" + str(i), "y" + str(i), "z" + str(i)]
                for i in range(1, n_atoms + 1)
            ]
            col_names = sum(col_names, [])
            print("Distance units: Angstrom")
            df = pd.DataFrame(
                self.xyz.reshape(-1, n_atoms * 3), columns=col_names
            )
            df = self._insert_traj_time(df)

            if self.rmsd is not None:
                df["RMSD"] = self.rmsd

            self.dataset = df

            print("\n-------------------------------------------------  ")
            print("  The size of the XYZ coordinates data set is\n   ")
            print("        Number of geometries = {}".format(df.shape[0]))
            print("          Number of features = {}".format(df.shape[1]))
            print("-------------------------------------------------  \n")

        else:
            print("---------------------------------------")
            print("The coordinates variable is empty!")
            print("There is no data available to save.")
            print("Please run the loader functions first.")
            print("---------------------------------------")

    @staticmethod
    def from_h5(outfile: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Read the XYZ coordinates of the MD frames generated by Newton-X NS.

        :param outfile: name of the NX output file, usually dyn.h5
        :type outfile: str
        :return: tuple containing an array of strings defining the atom labels,
                 a tensor of size (n_steps, n_atoms, 3) with all XYZ
                 coordinates of a single MD trajectory, and an array with the
                 time steps.
        :rtype: tuple in the form (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        """
        data = h5py.File(outfile, "r")
        path_to_coords = "particles/all/position/value"
        xyz_geoms = np.array(data[path_to_coords], dtype=np.float64)
        path_to_labels = "particles/all/names"
        atom_labels = np.array(data[path_to_labels], dtype="U")
        path_to_time = "particles/all/position/time"
        t_list = np.array(data[path_to_time], dtype=np.float64)

        return (atom_labels, xyz_geoms, t_list)

    def from_dyn(
        self, outfile: str, tmax=100000
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Get XYZ coordinates from the dyn.out file of classical Newton-X.

        :param outfile: name of the NX output file, dyn.out
        :type outfile: str
        :return: tuple containing an array of strings defining the atom labels,
                 a tensor of size (n_steps, n_atoms, 3) with all XYZ
                 coordinates of a single MD trajectory, and an array with the
                 time steps.
        :rtype: tuple in the form (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        """
        read_coords = False
        t = -1
        current_time = -1
        num_atoms = np.inf
        t_list = []

        xyz_geoms = []
        append_geoms = xyz_geoms.append
        atom_labels = []
        counter = 0

        if self.labels is not None:
            num_atoms = len(self.labels)
            atom_labels = self.labels

        with open(outfile, "r") as dyn_out:
            for line in dyn_out:
                # By keeping track of the current time we can deal with
                # repetitions in restart calculations, but also to deal
                # with switching in QM/MM dynamics.
                if "TIME" in line:
                    current_time = np.float64(line.split()[-2])
                    if current_time > tmax:
                        break

                if read_coords:
                    vals = line.split()
                    if len(vals) == 6:
                        counter += 1
                        if counter > num_atoms:
                            counter = 0
                            read_coords = False
                            continue
                        coords = np.array(vals[2:5], dtype=np.float64)
                        # xyz_geoms.append(coords)
                        append_geoms(coords)
                        # Labels will be read only in the first iteration
                        if current_time == 0.0 and isinstance(
                            atom_labels, list
                        ):
                            atom_labels.append(vals[0].upper())
                    else:
                        t_list.append(current_time)
                        read_coords = False
                        counter = 0

                # This condition is used to skip repeated geometries.
                if "geometry" in line:
                    if current_time == t:
                        read_coords = False
                    else:
                        read_coords = True
                    t = current_time

        num_atoms = len(atom_labels)

        xyz_geoms = np.array(xyz_geoms)
        # pylint: disable=too-many-function-args
        xyz_geoms = xyz_geoms.reshape(-1, num_atoms, 3)
        # pylint: enable=too-many-function-args
        xyz_geoms *= BOHR_TO_ANG
        atom_labels = np.asarray(atom_labels)
        t_list = np.asarray(t_list)

        return (atom_labels, xyz_geoms, t_list)

    @staticmethod
    def from_xyz(xyzfile) -> Tuple[np.ndarray, np.ndarray]:
        """Get XYZ coordinates from the dyn.xyz file of classical Newton-X.

        This method has the same parameters and return type as in
        :meth:`~ulamdyn.GetCoords.from_dyn`.
        """
        count = 0
        xyz_geoms = []
        atom_labels = []

        with open(xyzfile, "r") as inp:
            for line in inp:
                vals = line.split()

                if len(vals) == 1 and count == 0:
                    num_atoms = int(vals[0])

                if len(vals) >= 4:
                    coords = np.array(vals[-3:], dtype=np.float64)
                    coords = coords.reshape(-1, 3)
                    xyz_geoms.append(coords)
                    if count <= num_atoms + 2:
                        atom_labels.append(vals[0])

                count += 1

        atom_labels = np.asarray(atom_labels)
        xyz_array = np.vstack(xyz_geoms).reshape(-1, num_atoms, 3)

        return (atom_labels, xyz_array)

    def read_all_trajs(self, calc_rmsd: bool = True) -> None:
        """Concatenate the XYZ coordinates read from all MD trajectories.

        After running this method, the class attributes
        :attr:`~ulamdyn.GetCoords.labels` and :attr:`~ulamdyn.GetCoords.xyz`
        will be updated.
        """
        traj_id = []
        t_vals = []
        all_geoms = []

        # Instanciate the append before the loop for better efficiency
        append_trajs = traj_id.append
        append_times = t_vals.append
        append_all_geoms = all_geoms.append

        # Initiate the variable `ts` (timestamps) as an empty array to avoid
        # pylint error E606 (temporary solution).
        ts = np.array([])

        for trj in self.trajectories:
            tmax = self.trajectories.get(trj)
            results_dir = trj + "/RESULTS/"
            files = os.listdir(results_dir)
            h5file = next((f for f in files if f.endswith(".h5")), None)
            if h5file:
                h5file = results_dir + h5file
                atom_labels, xyz, ts = self.from_h5(h5file)
            elif os.path.isfile(results_dir + "dyn.out"):
                dynfile = results_dir + "dyn.out"
                atom_labels, xyz, ts = self.from_dyn(dynfile, tmax)
            elif os.path.isfile(results_dir + "dyn.xyz"):
                xyzfile = results_dir + "dyn.xyz"
                atom_labels, xyz = self.from_xyz(xyzfile)
                append_all_geoms(xyz)
            else:
                print("\nNX output file not found.")
                print("Check the directory %s" % trj + "/RESULTS" + "\n")
                continue
            append_all_geoms(xyz)
            append_times(ts)
            n_steps = len(ts)
            current_traj = np.int64(trj.replace("TRAJ", ""))
            append_trajs(np.full(n_steps, current_traj, dtype=np.int64))

        t_vals = np.concatenate(t_vals)
        traj_id = np.concatenate(traj_id)
        self.traj_time = np.concatenate(
            [traj_id.reshape(-1, 1), t_vals.reshape(-1, 1)], axis=1
        )
        self.xyz = np.concatenate(all_geoms, axis=0)
        # Important: capitalize the labels coming from h5 file (NX NS).
        self.labels = np.asarray([s.upper() for s in atom_labels])

        if calc_rmsd:
            self.align_geoms()

    def read_eq_geom(self) -> None:
        """Read the XYZ coordinates of a reference geometry.

        .. note:: This method should be executed before calculating the RMSD
                  with the :meth:`~ulamdyn.GetCoords.align_geoms` function.

        A file with name geom.xyz must be provided in the working directory
        (TRAJECTORIES). After reading the coordinates, the method will update
        the class attributes :attr:`~ulamdyn.GetCoords.labels` and
        :attr:`~ulamdyn.GetCoords.eq_xyz`.
        """
        try:
            atom_labels, ref_geom = self.from_xyz("geom.xyz")
            self.eq_xyz = np.squeeze(ref_geom, axis=0)
            self.labels = atom_labels
        except FileNotFoundError:
            print(
                "\n-----------------------------------------------------------"
            )
            print("Reference geometry not found!")
            print("The reference geometry is required to compute the RMSD. ")
            print(
                "Check if the geom.xyz file is available in the current dir."
            )
            print(
                "-----------------------------------------------------------\n"
            )

    def align_geoms(self, xyz_data=None) -> None:
        """Calculate the RMSD between the current and reference geometries.

        .. note:: Before calculating the RMSD, the method uses the Kabsch
                  algorithm to find the optimal alignment between the loaded
                  molecular geometry for each time step t and the reference
                  geometry.

        The calculated RMSD values will be stored in the class attribute
        :attr:`~ulamdyn.GetCoords.rmsd_values`, while the
        :attr:`~ulamdyn.GetCoords.xyz` attribute will be updated with the
        aligned geometries.
        """
        if self.eq_xyz is None:
            self.read_eq_geom()

        self.eq_xyz -= rmsd.centroid(self.eq_xyz)

        if xyz_data is None:
            if self.xyz is not None:
                xyz_data = self.xyz.copy()
            else:
                error_msg = (
                    "---------------------------------------------------"
                    + "\n "
                )
                error_msg += "XYZ coordinates not loaded!" + "\n "
                error_msg += (
                    "Please make sure that the read_all_trajs function "
                )
                error_msg += "has been executed." + "\n "
                error_msg += (
                    "---------------------------------------------------"
                    + "\n "
                )
                print(error_msg)
                return

        dim = xyz_data.shape
        aligned_geoms = np.empty(dim, dtype=np.float64)
        rmsd_values = np.empty((dim[0]), dtype=np.float64)

        for idx, geom in enumerate(xyz_data):
            geom -= rmsd.centroid(geom)
            U = rmsd.kabsch(geom, self.eq_xyz)
            geom = np.dot(geom, U)
            geom_rmsd = rmsd.rmsd(self.eq_xyz, geom)
            aligned_geoms[idx] = geom
            rmsd_values[idx] = geom_rmsd

        self.xyz = aligned_geoms
        self.rmsd = rmsd_values


# %% Starting new class: GetGradients
class GetGradients(BaseClass):
    """Class used to collect QM gradients from Newton-X MD trajectories.

    This class does not require arguments in its constructor. The outputs
    generate by the class is given in eV/angstrom.

    Data attributes:
    ----------------
       ``trajectories`` (dict): contain indices of the trajectories available
                                with the respective maximum time to read.\n
       ``all_grads`` (dict): store the gradient matrices (per state) for all
                             MD trajectories.\n
       ``dataset`` (dict): dictionary of dataframes with the flattened gradient
                           matrices.
    """

    # Defining slots to optimize memory access performance (RAM):
    __slots__ = ["trajectories", "all_grads", "datasets"]

    def __init__(self) -> None:
        """Class initializer."""
        # This variable contains a dictionary storing the available TRAJs
        # as keys together with the corresponding t_max:
        # {'TRAJ1': tmax_1, 'TRAJ2': tmax_2,..., 'TRAJN': tmax_n}
        self.trajectories = check_nx_trajs()
        # Store the gradient matrices per state as a dictionary, where
        # the key is the state identifier (e.g., {'S1': np.array})
        self.all_grads = dict()
        self.datasets = dict()

    @staticmethod
    def from_h5(outfile) -> dict:
        """Read XYZ gradients from a single trajectory generated by Newton-X NS

        :param outfile: name of the NX output file in the .h5 format,
                        usually dyn.h5.
        :type outfile: str
        :return: stacked XYZ gradients for all steps of a single MD trajectory,
                 returned as as a dictionary of tensors (numpy.ndarray, one for
                 each state) with shape (n_steps, n_atoms, 3).
        :rtype: dict
        """
        grads_dict = dict()
        data = h5py.File(outfile, "r")
        path_to_grads = "particles/all/gradients/value"
        xyz_grads = np.array(data[path_to_grads], dtype=np.float64)
        n_states = xyz_grads.shape[-1]

        for state in range(n_states):
            grads_dict[state] = (
                xyz_grads[:, :, :, state] * HARTREE_TO_eV / BOHR_TO_ANG
            )

        return grads_dict

    @staticmethod
    def from_nxlog(outfile, tmax=100000) -> dict:
        """Read XYZ gradients from one trajectory generated by Newton-X CS.

        This method has the same parameters and return type as in
        :meth:`~ulamdyn.GetGradients.from_h5`.

        :param outfile: Output file from Newton-X MD trajectory, namely nx.log.
        :type outfile: str
        :return: dictionary of numpy.ndarray with shape (n_steps, n_atoms, 3),
                 where the keys correspond to the state's id.
        :rtype: dict
        """
        read_grads = False
        count_start = 0
        t_current = 0
        t_hop = np.inf
        num_atoms = None

        # Dictionary used to store the gradients for each state
        grads_dict = dict()

        with open(outfile, "r") as nx_log:
            for line in nx_log:
                # This condition is used to monitor the MD restarting, and
                # then skip the repeated gradients (Step 0 of each restart)
                if "STARTING MOLECULAR DYNAMICS" in line:
                    count_start += 1

                if "Nat" in line:
                    num_atoms = np.int64(line.split()[-1])

                if "STEP" in line:
                    line_list = line.replace(",", "").split()
                    t_current = np.float64(line_list[4])
                    if t_current == tmax:
                        break

                if "Time of hopping" in line:
                    t_hop = np.float64(line.split()[-2])

                # The second condition in the if statement is used to skip
                # the gradients recalculated after hopping
                if ("Gradient" in line) and (t_current != t_hop):
                    if "current" in line:
                        continue
                    elif "state" in line:
                        state = "S" + line.split()[-2]
                    else:
                        state = "current_state"

                    if count_start == 2:
                        read_grads = False
                        count_start -= 1
                    else:
                        read_grads = True

                    if state not in grads_dict:
                        grads_dict[state] = list()
                    continue

                if read_grads:
                    vals = line.split()
                    if len(vals) == 3:
                        grads = np.array(vals, dtype=np.float64)
                        grads_dict[state].append(grads)
                    else:
                        read_grads = False

        for k in grads_dict.keys():
            grads_dict[k] = np.array(grads_dict[k])
            grads_dict[k] = grads_dict[k].reshape(-1, num_atoms, 3)
            grads_dict[k] *= HARTREE_TO_eV / BOHR_TO_ANG

        return grads_dict

    def read_all_trajs(self) -> None:
        """Read gradients from all MD trajectories and store into a
        dictionary."""
        all_grads = {}
        for trj in self.trajectories:
            print("Reading gradients from %s" % trj + "...")
            tmax = self.trajectories.get(trj)
            files = os.listdir(trj + "/RESULTS")
            h5file = next((f for f in files if f.endswith(".h5")), None)
            if h5file:
                h5file = trj + "/RESULTS/" + h5file
                gradients = self.from_h5(h5file)
            elif os.path.isfile(trj + "/RESULTS/nx.log"):
                nxfile = trj + "/RESULTS/nx.log"
                gradients = self.from_nxlog(nxfile, tmax)
            else:
                print("\nOutput file nx.log or *.h5 not available.")
                print("Check the directory %s" % trj + "/RESULTS" + "\n")

            if not all_grads:
                all_grads = {key: [] for key in gradients.keys()}
            for k in all_grads.keys():
                all_grads[k].append(gradients[k])

        for k in all_grads.keys():
            self.all_grads[k] = np.concatenate(all_grads[k], axis=0)

    def build_dataframe(self, save_csv=False) -> None:
        """Generate a dataset (pandas.DataFrame object) with all gradients.

        The XYZ matrices with the gradients of each molecular geometry is
        flattened into a one dimensional vector, such that every row of the
        dataset corresponds to one step of the MD trajectories.

        :param save_csv: if True enable the output of all gradient dataframes
                         in a csv format, defaults to False. The default name
                         of the saved dataset is all_gradients_s[n].csv, where
                         *n* is the number of the state.
        :type save_csv: bool, optional
        """
        if not self.all_grads:
            self.read_all_trajs()

        state = list(self.all_grads.keys())[0]
        n_atoms = self.all_grads[state].shape[1]
        col_names = [
            ["Gx" + str(i), "Gy" + str(i), "Gz" + str(i)]
            for i in range(1, n_atoms + 1)
        ]
        col_names = sum(col_names, [])

        for k in self.all_grads.keys():
            df = pd.DataFrame(
                self.all_grads[k].reshape(-1, n_atoms * 3), columns=col_names
            )
            self.datasets[k] = df
            if save_csv:
                output = "all_gradients_" + str(k).lower() + ".csv"
                df.to_csv(output, index=False, header=True)


# %% Starting new class: GetCouplings
class GetCouplings(BaseClass):
    """Class used to read the Nonadiabatic Coupling Vectors (NAC) from the
    MD trajectories.

    This class does not require arguments in its constructor. The outputs
    generate by the class is given in atomic units.

    Data attributes:
    ----------------
       ``trajectories`` (dict): contain indices of the trajectories available
                                with the respective maximum time to read.\n
       ``all_nacs`` (dict): store the NAC matrices (per state) for all
                            MD trajectories.\n
       ``dataset`` (dict): dictionary of dataframes with the flattened gradient
                           matrices.
    """

    # Define slots to optimize RAM memory access
    __slots__ = ["trajectories", "all_nacs", "datasets"]

    def __init__(self):
        """Class initializer."""
        # This variable contains a dictionary storing the available
        # trajectories as keys together with the corresponding t_max:
        # {'TRAJ1': tmax_1, 'TRAJ2': tmax_2,..., 'TRAJN': tmax_n}
        self.trajectories = check_nx_trajs()
        # Store the NAC matrices per state as a dictionary, where
        # the key is an state pair ID (e.g., {'S2S1': np.array})
        self.all_nacs = dict()
        self.datasets = dict()

    @staticmethod
    def from_h5(outfile) -> dict:
        """Read NACs from one trajectory generated by the new Newton-X.

        :param outfile: name of the NX output file in the .h5 format,
                        usually dyn.h5.
        :type outfile: str
        :return: stacked nonadiabatic coupling vectors (NAC) for all steps of
                 a single MD trajectory, provided as a dictionary of tensors
                 (numpy.ndarray, one for each state) with shape
                 (n_steps, n_atoms, 3).
        :rtype: dict
        """
        nacs_dict = dict()
        data = h5py.File(outfile, "r")
        if "nad" in data["particles/all"].keys():
            path_to_nacs = "particles/all/nad/value"
        else:
            print("NACs not available!")
            print("Check the NX outputs.")
            return nacs_dict

        nacs = np.array(data[path_to_nacs], dtype=np.float64)
        # Temporary solution.
        epot = np.array(
            data["observables/potential_energy/value"], dtype=np.float64
        )
        num_blocks = nacs.shape[-1]
        num_states = epot.shape[-1]
        if num_blocks == 1:
            num_blocks += 1
        states = [
            "S" + str(j + 1) + "S" + str(i + 1)
            for i, j in combinations(range(num_states), 2)
        ]

        for n, state in enumerate(states):
            nacs_dict[state] = nacs[:, :, :, n]

        return nacs_dict

    @staticmethod
    def from_nxlog(outfile: str, tmax=100000) -> dict:
        """Read NACs from one trajectory generated by the Newton-X CS."""
        read_nacs = False
        count_start = 0

        # Initiate variables with value None to avoid lint errors (temporary
        # solution). Ideally, this value should be available before starting
        # to process the NX output file.
        num_atoms = None
        states = None

        nacs_dict = dict()

        with open(outfile, "r") as nx_log:
            for line in nx_log:
                if "STARTING MOLECULAR DYNAMICS" in line:
                    count_start += 1

                if "STEP" in line:
                    line_list = line.replace(",", "").split()
                    t_current = np.float64(line_list[4])
                    if t_current == tmax:
                        break

                if "Nat" in line:
                    num_atoms = np.int64(line.split()[-1])

                if "nstat " in line:
                    num_states = np.int64(line.split()[-1])
                    num_blocks = np.int64(num_states * (num_states - 1) / 2)
                    if num_blocks == 1:
                        num_blocks += 1
                    states = [
                        "S" + str(j + 1) + "S" + str(i + 1)
                        for i, j in combinations(range(num_states), 2)
                    ]

                if "phase adjustment" in line:
                    if count_start == 2:
                        read_nacs = False
                        count_start -= 1
                    else:
                        read_nacs = True
                        count_line = 0
                        nacs_step = np.full(
                            (num_atoms * num_blocks, 3), np.nan
                        )
                        continue

                if read_nacs:
                    vals = line.split()
                    if len(vals) == 3:
                        atom_nac = np.array(vals, dtype=np.float64)
                        nacs_step[count_line] = atom_nac
                        count_line += 1
                    else:
                        nacs_step = nacs_step.reshape(num_blocks, num_atoms, 3)
                        for i, s in enumerate(states):
                            if s not in nacs_dict.keys():
                                nacs_dict[s] = []
                            nacs_dict[s].append(nacs_step[i])
                        read_nacs = False

        for k in nacs_dict.keys():
            nacs_dict[k] = np.array(nacs_dict[k])
            nacs_dict[k] = nacs_dict[k].reshape(-1, num_atoms, 3)

        return nacs_dict

    def read_all_trajs(self) -> None:
        """Read NACs from all MD trajectories and store into a dictionary."""
        all_nacs = dict()
        for trj in self.trajectories:
            print("Reading nonadiabatic couplings from %s" % trj + "...")
            tmax = self.trajectories.get(trj)
            files = os.listdir(trj + "/RESULTS")
            h5file = next((f for f in files if f.endswith(".h5")), None)
            if h5file:
                h5file = trj + "/RESULTS/" + h5file
                nacs = self.from_h5(h5file)
            elif os.path.isfile(trj + "/RESULTS/nx.log"):
                nxfile = trj + "/RESULTS/nx.log"
                nacs = self.from_nxlog(nxfile, tmax)
            else:
                print("\nOutput file nx.log or *.h5 not available.")
                print("Check the directory %s" % trj + "/RESULTS" + "\n")

            if not all_nacs:
                all_nacs = {key: list() for key in nacs.keys()}
            for k in all_nacs.keys():
                all_nacs[k].append(nacs[k])

        for k in all_nacs.keys():
            self.all_nacs[k] = np.concatenate(all_nacs[k], axis=0)

    def build_dataframe(self, save_csv=False) -> None:
        """Generate a dataset (pandas.DataFrame object) with all NACs.

        The XYZ matrices with the nonadiabatic couplings obtained for each
        molecular geometry is flattened into a one dimensional vector. In this
        way, every row of the dataset corresponds to one step of a given MD
        trajectory.

        :param save_csv: if True enable the output of all NAC dataframes in a
                         csv format, defaults to False. The default name of the
                         exported dataset is all_nacs_s[n].csv, where *n* is
                         the number of the state.
        :type save_csv: bool, optional
        """
        if not self.all_nacs:
            self.read_all_trajs()

        state = list(self.all_nacs.keys())[0]
        n_atoms = self.all_nacs[state].shape[1]
        col_names = [
            ["nac_x" + str(i), "nac_y" + str(i), "nac_z" + str(i)]
            for i in range(1, n_atoms + 1)
        ]
        col_names = sum(col_names, [])

        for k in self.all_nacs.keys():
            df = pd.DataFrame(
                self.all_nacs[k].reshape(-1, n_atoms * 3), columns=col_names
            )
            self.datasets[k] = df
            if save_csv:
                output = "all_nacs_" + k.lower() + ".csv"
                df.to_csv(output, index=False, header=True)


# %% Starting new class: GetProperties
class GetProperties(BaseClass):
    """Read all properties available in the Newton-X MD trajectories.

    .. note:: This class does not require arguments in its constructor. All the
              energy quantities processed by the class are transformed from Ha
              to eV. For the other properties, the original units used in
              Newton-X are kept.

    Data attributes:
    ----------------
       ``trajectories`` (dict): trajectories ID (TRAJXX) found in the working
                                directory are stored as keys of the
                                dictionary, and the corresponding values are
                                the maximum simulation time to be read.\n
       ``dataset`` (pd.dataframe): store a dataframe with all properties.\n
       ``num_states`` (int): keep track of the number of states considered in
                             the MD simulation.\n
       ``nx_version`` (str): identify the Newton-X version, can be either cs
                             (classical series) or ns (new series).\n

    """

    __slots__ = ["trajectories", "dataset", "num_states", "nx_version"]

    def __init__(self) -> None:
        """Class initializer."""
        # This variable contains a dictionary storing the available
        # trajectories as keys together with the corresponding t_max:
        # {'TRAJ1': tmax_1, 'TRAJ2': tmax_2,..., 'TRAJN': tmax_n}
        self.trajectories = check_nx_trajs()
        traj = "TRAJ1"
        if self.trajectories:
            traj = list(self.trajectories)[0]
        # This class variable will be used to store a dataframe
        # with all properties read from the NX outputs
        self.dataset = None
        # Auxiliary variable to keep track of the number of states.
        # The default value will be updated in the energy function.

        # TO-DO: Create a function to read the number of states either from the
        # control.dyn file or from RESULTS/nx.log
        self.num_states = int(read_nx_control(traj).get("nstat", None))
        # This variable is used to recognize the version of Newton-X
        # (cs or ns), and then use this information to decide which functions
        # should be used to collect the properties data.
        self.nx_version = get_nx_version(traj)

    @property
    def save_csv(self) -> None:
        """Save the dataset with QM properties read from the NX trajectories.

        The following properties are included in the dataset:

        + trajectory index;
        + simulation time;
        + total energy;
        + energy gaps between states (eV);
        + oscillator strength (if available);
        + states population.
        + norm of the nonadiabatic coupling matrices (if available);
        + three highest MCSCF coefficients per state (only for NX/Columbus);
        """
        if self.dataset is not None:
            df = self.dataset.copy()
            df["time"] = df["time"].astype(object)

            print("                                             ")
            print("------------------------------------------------")
            print("  The size of the properties data set is\n  ")
            print("    Number of geometries = {}".format(df.shape[0]))
            print("     Number of features = {}".format(df.shape[1]))
            print("------------------------------------------------\n")

            df.to_csv(
                "all_properties.csv",
                index=False,
                header=True,
                float_format="%.10f",
            )

        else:
            print("---------------------------------------")
            print("The properties variable is empty!")
            print("There is no data to save.")
            print("Please run the loader functions first.")
            print("---------------------------------------\n")

    def _update_properties(self, df):
        # From now on, the input dataframe df must always contain the TRAJ and
        # time columns. These columns are necessary to identify/export the
        # differences between the two dfs.
        if self.dataset is None:
            print("\n-----------------------------------------------------")
            print("The properties dataset is empty.")
            print("Updating class variable with the current loaded data.")
            print("-----------------------------------------------------\n")
            self.dataset = df

        if all(isinstance(i, pd.DataFrame) for i in (self.dataset, df)):
            if self.dataset.shape[0] == df.shape[0]:
                current_cols = self.dataset.columns.tolist()
                cols_to_add = df.columns.difference(
                    self.dataset.columns
                ).tolist()
                if len(cols_to_add) != 0:
                    df = df[cols_to_add]
                    if "time" in current_cols:
                        dfs_to_merge = (self.dataset, df)
                    else:
                        dfs_to_merge = (df, self.dataset)
                    self.dataset = pd.concat(
                        dfs_to_merge, axis=1, ignore_index=False
                    )
            else:
                select_cols = ["TRAJ", "time"]
                df_diff = pd.concat(
                    [self.dataset[select_cols], df[select_cols]]
                ).drop_duplicates(keep=False)
                warning = "*************************************************\n"
                warning += (
                    "WARNING: The size of the dataframes does not match!\n\n"
                )
                warning += "    Dataset 1 contains {} rows\n".format(
                    self.dataset.shape[0]
                )
                warning += "    Dataset 2 contains {} rows\n\n".format(
                    df.shape[0]
                )
                warning += "Please check the mismatched TRAJ and time values\n"
                warning += "in the properties_diff.csv file.\n"
                warning += "*************************************************"
                print(warning)
                df_diff.to_csv("properties_diff.csv", index=False, header=True)

    def _energies_from_h5(self) -> np.ndarray:
        all_energies = list()
        traj_id = list()

        for trj in self.trajectories:
            data = read_h5_nx(trj)
            if data is None:
                continue

            time = np.array(
                data["observables/potential_energy/time"], dtype=np.float64
            )
            current_state = np.array(
                data["observables/nstatdyn/value"], dtype=np.int64
            )
            epot = np.array(
                data["observables/potential_energy/value"], dtype=np.float64
            )
            etot = np.array(
                data["observables/total_energy/value"], dtype=np.float64
            )

            time = time.reshape(-1, 1)
            etot = etot.reshape(-1, 1)
            current_state = current_state.reshape(-1, 1)

            data = np.concatenate((time, epot, etot, current_state), axis=1)

            n_samples = data.shape[0]
            idx = np.int64(trj.replace("TRAJ", ""))
            traj_id.append(np.full(n_samples, idx, dtype=np.int64))
            all_energies.append(data)

        traj_id = np.concatenate(traj_id)
        traj_id = traj_id.reshape(-1, 1)
        all_energies = np.concatenate(all_energies, axis=0)
        all_energies[:, 1:-1] *= HARTREE_TO_eV

        # The final order of the columns in the array should be like:
        # ['time', 'potential energies', 'total energy', 'current state',
        # 'TRAJ']
        all_energies = np.concatenate((all_energies, traj_id), axis=1)

        return all_energies

    def _energies_from_dat(self) -> np.ndarray:
        all_energies = []
        append_energies = all_energies.append
        traj_id = []
        append_traj_id = traj_id.append

        for trj in self.trajectories:
            print("Reading energies from %s" % trj + "...")
            try:
                enfile = trj + "/RESULTS/en.dat"
                en = np.loadtxt(enfile)
            except FileNotFoundError:
                print("\n-------------------------------------")
                print("Energy file not found.")
                print("Check the %s/RESULTS directory." % trj)
                print("-------------------------------------\n")
                continue

            tmax = self.trajectories.get(trj, 100000)
            mask = en[:, 0] <= tmax
            en = en[mask]
            append_energies(en)
            n_samples = en.shape[0]
            idx = np.int64(trj.replace("TRAJ", ""))
            append_traj_id(np.full(n_samples, idx, dtype=np.int64))

        traj_id = np.concatenate(traj_id)
        traj_id = traj_id.reshape(-1, 1)
        all_energies = np.concatenate(all_energies, axis=0)
        all_energies[:, 1:] *= HARTREE_TO_eV

        # After swapping the columns, the expected order of the columns should
        # be like:
        # ['time', 'potential energies', 'total energy', 'current state']
        all_energies[:, -1], all_energies[:, -2] = (
            all_energies[:, -2],
            all_energies[:, -1].copy(),
        )
        # And finally we add the column with the indices of the trajectories
        all_energies = np.concatenate((all_energies, traj_id), axis=1)

        return all_energies

    def energies(self):
        """Read / process the energy information from the en.dat (classical NX)
        or .h5 (new NX) file.

        :return: a processed dataset with the information of all trajectories
                 stacked, and containing the following columns "TRAJ", "time",
                 "State", "Total_Energy" plus the energy gaps between the
                 accessible states (e.g., DE12) and binary columns to identify
                 the hopping points (e.g., Hops_S21).
        :rtype: pandas.DataFrame | modin.pandas.dataframe.DataFrame
        """
        if self.nx_version == "cs":
            all_energies = self._energies_from_dat()
        elif self.nx_version == "ns":
            all_energies = self._energies_from_h5()
        else:
            print("ERROR:")
            print("Newton-X version not recognized!")
            return

        self.num_states = all_energies.shape[1] - 4
        state_labels = ["S" + str(i + 1) for i in range(self.num_states)]
        col_names = (
            ["time"] + state_labels + ["Total_Energy", "Current_State", "TRAJ"]
        )
        df = pd.DataFrame(all_energies, columns=col_names)
        df["TRAJ"] = df["TRAJ"].astype(int)

        if self.nx_version == "cs":
            df["State"] = (
                df[state_labels].eq(df["Current_State"], axis=0).idxmax(1)
            )
            df["State"] = df["State"].str.replace("S", "").astype(int)
        elif self.nx_version == "ns":
            df["State"] = df["Current_State"].astype(int)

        # Auxiliary variable used to find the hopping points
        df["State_Next"] = df.groupby(by="TRAJ")["State"].shift(
            -1, fill_value=-10
        )

        # Loop to calculate energy difference between all possible pair of
        # states
        for i, j in combinations(state_labels, 2):
            from_to = j.replace("S", "") + i.replace("S", "")
            new_col = "DE" + from_to
            df[new_col] = df[j] - df[i]
            si = np.int64(i.replace("S", ""))
            sj = np.int64(j.replace("S", ""))
            # Create a binary column to identify hopping geometries
            # The first condition corresponds to hoppings by state decay
            new_col = "Hops_S" + str(sj) + str(si)
            condition = (df["State"] == sj) & (df["State_Next"] == si)
            df[new_col] = np.where(condition, 1, 0)
            # The second condition takes into account upward hoppings
            new_col = "Hops_S" + str(si) + str(sj)
            condition = (df["State"] == si) & (df["State_Next"] == sj)
            df[new_col] = np.where(condition, 1, 0)

        cols_to_drop = state_labels[1:] + ["Current_State", "State_Next"]
        df.drop(cols_to_drop, axis=1, inplace=True)

        # Remove all columns that contains only zeros
        df = df.loc[:, (df != 0).any(axis=0)]

        all_cols = df.columns.tolist()
        base_cols = ["TRAJ", "time", "State", "Total_Energy"]
        cols_reordered = base_cols + list(set(all_cols) - set(base_cols))
        df = df[cols_reordered]

        self._update_properties(df)

        return df

    def _os_from_h5(self):
        all_osc_strengths = []
        traj_id = []
        times = []
        append_times = times.append
        append_trajs = traj_id.append

        for trj in self.trajectories:
            data = read_h5_nx(trj)
            if data is None:
                continue
            if "oscillator_strengths" not in data["observables"].keys():
                return
            print("Reading oscillator strength from %s" % trj + "...")
            osc_traj = np.array(data["observables/oscillator_strengths/value"])
            t_vec = np.array(data["observables/oscillator_strengths/time"])
            n_states = osc_traj.shape[-1]
            all_osc_strengths.append(osc_traj)
            append_times(t_vec)
            n_steps = len(t_vec)
            current_traj = np.int64(trj.replace("TRAJ", ""))
            append_trajs(np.full(n_steps, current_traj, dtype=np.int64))

        if len(all_osc_strengths) != 0:
            all_osc_strengths = np.concatenate(all_osc_strengths, axis=0)
            traj_id = np.concatenate(traj_id, axis=0)
            times = np.concatenate(times, axis=0)

            col_names = [
                "f_0" + str(state) for state in range(1, n_states + 1)
            ]
            df = pd.DataFrame(all_osc_strengths, columns=col_names)
            df.insert(loc=0, column="TRAJ", value=traj_id)
            df.insert(loc=1, column="time", value=times)

            return df

    def _os_from_txt(self):
        osc_dict = dict()
        traj_id = []
        times = []

        for trj in self.trajectories:
            # step = -1
            read_line = False
            tmax = self.trajectories.get(trj, 100000)

            try:
                propfile = trj + "/RESULTS/properties"
                f = open(propfile, "r")
            except FileNotFoundError:
                print("\n---------------------------------------")
                print("Properties file not found or corrupted.")
                print("Check the %s/RESULTS directory." % trj)
                print("---------------------------------------\n")
                continue

            lines = f.read()
            if "Oscillator strength" not in lines:
                f.close()
                print("\nOscillator strength not found in %s" % propfile)
                return

            print("Reading oscillator strength from %s" % trj + "...")
            lines = lines.split("\n")

            # oscillator_lines = list(
            #    filter(lambda x: x.startswith(" Oscillator "), lines)
            # )

            # for line in oscillator_lines:
            #    states = line.split()[2]
            previous_state = np.int64(lines[1].split()[-1])
            # Start reading the properties file
            for line in lines:
                if "STEP:" in line:
                    t_current = np.float64(line.split()[2])
                    if t_current > tmax:
                        break
                    # current_step = np.int64(line.split()[4])
                    current_state = np.int64(line.split()[-1])

                    if current_state == previous_state:
                        read_line = True
                        count = 0
                    else:
                        read_line = False
                    # step = current_step
                    previous_state = current_state

                if ("Oscillator" in line) and read_line:
                    count += 1
                    x = np.float64(line.split()[4])
                    states = line.split()[2]
                    osc_dict.setdefault(states, []).append(x)
                    if count == 1:
                        times.append(t_current)
                        traj_id.append(np.int64(trj.replace("TRAJ", "")))

            f.close()

        col_names = [
            "f_" + "".join(key.replace("(", "").replace(")", "").split(","))
            for key in osc_dict
        ]
        df = pd.DataFrame(osc_dict)
        df.columns = col_names
        traj_id = np.array(traj_id)
        times = np.array(times)
        df.insert(loc=0, column="TRAJ", value=traj_id)
        df.insert(loc=1, column="time", value=times)

        return df

    def oscillator_strength(self):
        """Collect the oscillator strength from properties file.

        .. note:: The oscillator strength (OSS) is not always available in the
                  output. If needed, check the Newton-Xdocumentation to see
                  what are the required keywords and methods. The OSS data can
                  be read from either classical or new series NX trajectories.

        :return: a dataset with the oscillator strength information read from
                 all available NX trajectories with one column for each
                 transition between states; if the class variable
                 :attr:`~ulamdyn.GetProperties.dataset` has been already
                 updated with some properties, the oscillator strength data
                 will be merged
                 with the existing properties dataset.
        :rtype: pandas.DataFrame | modin.pandas.dataframe.DataFrame
        """

        if self.nx_version == "cs":
            df = self._os_from_txt()
        elif self.nx_version == "ns":
            df = self._os_from_h5()
        else:
            print("ERROR:")
            print("Newton-X version not recognized!")
            return

        self._update_properties(df)

        return df

    def _populations_from_h5(self):
        all_populations = []
        traj_id = []
        times = []
        append_pop = all_populations.append
        append_times = times.append
        append_trajs = traj_id.append

        for trj in self.trajectories:
            print("Reading populations from %s" % trj + "...")
            data = read_h5_nx(trj)
            if data is None:
                continue

            pop_traj = np.array(
                data["observables/populations/value"], dtype=np.float64
            )
            t_vec = np.array(
                data["observables/populations/time"], dtype=np.float64
            )
            n_steps = len(t_vec)
            current_traj = np.int64(trj.replace("TRAJ", ""))

            append_pop(pop_traj)
            append_times(t_vec)
            append_trajs(np.full(n_steps, current_traj, dtype=np.int64))

        all_populations = np.concatenate(all_populations, axis=0)
        times = np.concatenate(times, axis=0)
        traj_id = np.concatenate(traj_id, axis=0)

        return (all_populations, times, traj_id)

    def _populations_from_txt(self):
        coefs_list = []
        traj_id = []
        times = []
        append_coefs = coefs_list.append

        for trj in self.trajectories:
            print("Reading populations from %s" % trj + "...")

            try:
                dynfile = trj + "/RESULTS/dyn.out"
                f = open(dynfile, "r")
            except FileNotFoundError:
                print("\n-------------------------------------------------")
                print("The file dyn.out was not found or is corrupted.")
                print("Check the %s/RESULTS directory." % trj)
                print("-------------------------------------------------\n")
                continue

            # step = -1
            current_step = 0
            error_en_conserv = False
            tmax = self.trajectories.get(trj, 100000)
            n_states = 0
            lines = f.readlines()
            # Start reading the properties file
            for line in lines:
                if "ERROR TERMINATION" in line:
                    error_en_conserv = True

                if "STEP" in line:
                    count = 0
                    # error_found = False
                    current_step = np.int64(line.split()[1])
                    t_current = np.float64(line.split()[-2])
                    if t_current > tmax:
                        break

                if " Wave function state " in line:
                    count += 1
                    coefs = list(np.float_(line.split()[-2:]))
                    if error_en_conserv:
                        coefs_list[-1] = coefs
                    else:
                        append_coefs(coefs)
                        if count == 1:
                            times.append(t_current)
                            traj_id.append(np.int64(trj.replace("TRAJ", "")))
                    if current_step == 0:
                        n_states += 1

                if "------------" in line:
                    error_en_conserv = False

        coefs_list = np.array(coefs_list, dtype=np.float64)
        all_populations = np.sum(coefs_list**2, axis=1).reshape(-1, n_states)
        traj_id = np.array(traj_id)
        times = np.array(times)

        return (all_populations, times, traj_id)

    def populations(self):
        """Read and calculate the population for each accessible state.

        .. note:: If the MD simulations were performed with the classical
                  Newton-X, the populations will be calculated using the
                  wavefunction coefficients. In the new Newton-X, the
                  populations are already calculated and available in the
                  .h5 file.

        :return: a dataset with the populations obtained from all available NX
                 trajectories with one column per state; if the class variable
                 :attr:`~ulamdyn.GetProperties.dataset` has been already
                 updated with some properties, the population data will be
                 merged with the existing dataset.
        :rtype: pandas.DataFrame | modin.pandas.dataframe.DataFrame
        """
        if self.nx_version == "cs":
            all_populations, times, traj_id = self._populations_from_txt()
        elif self.nx_version == "ns":
            all_populations, times, traj_id = self._populations_from_h5()
        else:
            print("ERROR:")
            print("Newton-X version not recognized!")
            return

        ncols = all_populations.shape[1]
        col_names = ["Pop" + str(i) for i in range(1, ncols + 1)]
        df = pd.DataFrame(all_populations, columns=col_names)
        df.insert(loc=0, column="TRAJ", value=traj_id)
        df.insert(loc=1, column="time", value=times)

        # df = df.drop_duplicates(subset=['TRAJ','time'], keep='last')

        self._update_properties(df)

        return df

    def nac_norm(self):
        """Calculate the norm of the nonadiabatic coupling matrices for each
        pair of states.

        After running this function, the class variable
        :attr:`~ulamdyn.GetProperties.dataset` will be updated with the loaded
        NACs norm data.

        :return: a dataset with the Frobenius norm of the nonadiabatic
                 coupling matrices of all NX trajectories calculated for each
                 possible transition between states; the number of columns in
                 the NACs norm dataset is given by n_states * (n_states - 1)/2.
        :rtype: pandas.DataFrame | modin.pandas.dataframe.DataFrame
        """

        nacs_norm = {}
        gnac = GetCouplings()
        gnac.read_all_trajs()

        if gnac.all_nacs:
            for k in gnac.all_nacs:
                norm = np.linalg.norm(gnac.all_nacs[k], axis=(1, 2))
                nacs_norm[k] = norm

            df = pd.DataFrame(nacs_norm)
            col_names = ["NAC_norm_" + k for k in nacs_norm]
            df.columns = col_names
            self._update_properties(df)

            return df

    def _mcscf_coefs_from_txt(self):
        all_mcscf_coefs = []
        traj_id = []
        times = []
        check_csf = "   csf       coeff       coeff**2    step(*)\n"

        for trj in self.trajectories:
            try:
                nxlog = trj + "/RESULTS/nx.log"
                f = open(nxlog, "r")
            except FileNotFoundError:
                print("\n-------------------------------------------------")
                print("The file nx.log was not found or is corrupted.")
                print("Check the %s/RESULTS directory." % trj)
                print("-------------------------------------------------\n")
                continue

            tmax = self.trajectories.get(trj, 100000)
            lines = f.readlines()

            if check_csf not in lines:
                print("\nMCSCF coefficients are not available in nx.log.\n")
                return (None, None, None)

            read_coefs = False
            # TO-DO: take this value from _init_
            # n_tot_state = int(
            #     list(
            #         filter(
            #             lambda x: "nstat " in x, lines[0:100]
            #         )
            #     )[0].split()[-1]
            # )
            t_current = 0
            replace_previous = False
            current_coefs = []

            # Start reading the properties file
            print("Reading MCSCF coefficients from %s" % trj + "...")

            for line in lines:
                # Note: in this logic, the file is not read in order from the
                #       beginning to the end. Therefore, the `current_coefs`
                #       must be created before to avoid lint error.
                if "FINISHING STEP" in line:
                    t_current = np.float64(line.split()[4])
                    if replace_previous:
                        times[-1] = t_current
                        traj_id[-1] = np.int64(trj.replace("TRAJ", ""))
                        all_mcscf_coefs[-1] = current_coefs
                    else:
                        times.append(t_current)
                        traj_id.append(np.int64(trj.replace("TRAJ", "")))
                        all_mcscf_coefs.append(current_coefs)
                    replace_previous = False
                    if t_current == tmax:
                        break
                # elif "Finished moldyn03 with ERROR" in line:
                elif "Restarting NX job" in line:
                    replace_previous = True

                if "Starting normal electronic structure calculation" in line:
                    current_coefs = []
                    continue

                if "csf       coeff" in line:
                    read_coefs = True
                    continue

                if len(line.split()) == 0:
                    read_coefs = False

                if read_coefs and "-----" not in line:
                    coef = np.float_(line.split()[2])
                    current_coefs.append(coef)

            f.close()

        all_mcscf_coefs = np.array(all_mcscf_coefs, dtype=np.float64)
        traj_id = np.array(traj_id)
        times = np.array(times)

        return (all_mcscf_coefs, times, traj_id)

    def mcscf_coefs(self):
        """Collect the squared coefficients of the MCSCF wavefunction.

        .. note:: This method works only for NAMD trajectories generated with
                  Columbus.

        :return: a dataset with the three highest MCSCF coefficients (the three
                 largest) for each electronic state read from the NX output per
                 state; if the class variable
                 :attr:`~ulamdyn.GetProperties.dataset` has been already
                 updated with some properties, the population data will be
                 merged with the existing dataset.
        :rtype: pandas.DataFrame | modin.pandas.dataframe.DataFrame
        """
        if self.nx_version == "cs":
            all_mcscf_coefs, times, traj_id = self._mcscf_coefs_from_txt()
        elif self.nx_version == "ns":
            # TO-DO: all_mcscf_coefs = self._mcscf_coefs_from_h5()
            print("This function is not available yet for the new NX.")
            return
        else:
            print("ERROR:")
            print("Newton-X version not recognized!")
            return

        if all_mcscf_coefs is not None:
            n_coefs = 3
            # TO-DO: n_states should be taken directly from _init_
            n_states = int(all_mcscf_coefs.shape[1] / n_coefs)
            states_id = ["S" + str(i + 1) for i in range(n_states)]
            coefs_id = ["c" + str(i + 1) for i in range(n_coefs)]
            col_names = list(map("_mcscf_".join, product(states_id, coefs_id)))
            df = pd.DataFrame(all_mcscf_coefs, columns=col_names)
            df.insert(loc=0, column="TRAJ", value=traj_id)
            df.insert(loc=1, column="time", value=times)

            self._update_properties(df)

        return self.dataset
