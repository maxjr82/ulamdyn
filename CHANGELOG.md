# Change Log
============

This file will be used to keep track and document all notable changes related to the ULaMDyn project.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.1.1 (2024-11-11)

### Fix

- **ci**: fix issue with version bump

## 1.1.0 (2024-11-11)

### Feat

- **descriptor**: implement soap descriptor

## 1.0.1 (2024-10-26)

### Fix

- **ci**: testing CI workflow for first release

## 1.0.0 (2021-07-25)

- First beta release

